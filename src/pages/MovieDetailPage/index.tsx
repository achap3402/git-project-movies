import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {useEffect} from "react";
import { useDispatch } from "react-redux";

import routeMain from './routes';

import { selectDetail } from "store/detail/selectors";
import { loadDetail } from "store/detail/actions";
import { TypedDispatch } from "store";
import { ID } from "types/ID";
import DateView from "components/DateView";
import Star from 'assets/img/star.png';

import './styles.scss';

const MovieDetailPage = () => {
    const {id} = useParams<ID>();
    const dispatch = useDispatch<TypedDispatch>();
    const movie = useSelector(selectDetail);

    useEffect(() => {
        dispatch(loadDetail(id));
    }, [id, dispatch])

    return (
        <section className="movieDetailWrapper">
            {movie ? (
                <div className="container">
                    <div className="image">
                        <img src={movie.image.medium} alt={movie.image.medium}/>
                    </div>
                    <div className="content">
                        <div className="contentTitle">
                            <div className="name">
                                <h2>{movie.name}</h2>
                            </div>
                            <div className="rating">
                                <div className="ratingImage">
                                    <img src={Star} alt={Star}/>
                                </div>
                                <div className="ratingNumber">
                                    {movie.rating.average === null ? (
                                        <span>0/10</span>
                                    ) : <span>{movie.rating.average}/10</span>}
                                </div>
                            </div>
                        </div>
                        <div className="contentBody">
                            <div className="contentBodyItem">
                                <h3>ГОД ВЫХОДА:</h3>
                                <div className="description"><DateView value={movie.premiered}/></div>
                            </div>
                            <div className="contentBodyItem">
                                <h3>СТРАНА:</h3>
                                <div className="description">{movie.network === null ? (<p></p>) : <p>{movie.network.country.name}</p>}</div>
                            </div>
                            <div className="contentBodyItem">
                                <h3>ЖАНР:</h3>
                                <div className="description">{movie.genres.join(", ")}</div>
                            </div>
                            <div className="contentBodyItem">
                                <h3>ОПИСАНИЕ:</h3>
                                <div className="description">{movie.summary}</div>
                            </div>
                        </div>
                    </div>
                </div>
            ) : <>данные не получены</>}
        </section>
    )
}

export {routeMain};
export default MovieDetailPage;