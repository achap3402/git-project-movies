const routeMain = (id = ':id') => `/movieDetail/${id}`;

export default routeMain;