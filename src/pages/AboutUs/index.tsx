import React from "react";

import routeMain from "./routes";

import About from 'assets/img/aboutUs.jpg';


import './styles.scss';

const AboutUs = () => {
    return (
        <section className="aboutUsWrapper">
            <div className="aboutUs">
                <div className="content">
                    <div className="contentTitle">
                        MOVIESinfo
                    </div>
                    <div className="contentBody">
                        <p><img src={About} alt={About}/>На нашем сайте вы сможете найти самые различные фильмы, любых жанров: комедии, драмы, ужасы, исторические, документальные и другие. Здесь найдется фильм для каждого. Это один из самых популярных сайтов с фильмами. И если Вы захотите смотреть кино именно здесь, это будет правильный выбор.</p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export {routeMain};

export default AboutUs;