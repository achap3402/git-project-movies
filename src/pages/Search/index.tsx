import React, {useState, useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";

import routeMain from "./routes";

import { TypedDispatch } from "store";
import { selectCategory } from "store/category/selectors";
import { loadCategory } from "store/category/actions";
import MovieSearch from "components/MovieSearch";
import Input from "components/Input";
import Button from "components/Button";

import './styles.scss';

const Search = () => {
    const dispatch = useDispatch<TypedDispatch>();
    const movies = useSelector(selectCategory);
    const [category, setCategory] = useState<string>("")
    const [value, setValue] = useState<string>("");

    useEffect(() => {
        dispatch(loadCategory(value));
    }, [dispatch, value])
    
    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setCategory(event.target.value)
    }
    
    const onKeyDown = (event: KeyboardEvent) => {
        if (event.key === "Enter") {
            setValue(category);
        }
    }
    
    const handleClick = () => {
        setValue(category);
    }

    return (
        <section className="searchWrapper">
            <div className="search">
                <div className="searchTitle">
                    <p>Поиск по категории</p>
                </div>
                <div className="searchEngine">
                    <Input
                        onChange={onChange}
                        onKeyDown={onKeyDown}
                        placeholder="Example: girls"
                    />
                    <Button
                        handleClick={handleClick}
                    />
                </div>
                <div className="searchResultTitle">
                    Результаты поиска:
                </div>
                <div className="searchResultContent">
                    <div className="searchResult">
                        {movies.length > 0 ? <MovieSearch list={movies}/> :
                            <p>По Вашему запросу<br />ничего не найдено!</p>
                        }
                    </div>
                </div>
            </div>
        </section>
    )
}

export {routeMain};

export default Search;