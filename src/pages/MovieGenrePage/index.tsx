import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import routeMain from "./routes";

import { loadMovieGenres } from "store/genres/actions";
import { selectGenre } from "store/genres/selectors";
import { TypedDispatch } from "store";
import MovieListGenre from "components/MovieListGenre";

const MovieGenrePage = () => {
    const dispatch = useDispatch<TypedDispatch>();
    const movieGenre = useSelector(selectGenre);
    useEffect(() => {
        dispatch(loadMovieGenres());
    }, [dispatch])
    return (
        <section className="MovieGenrePage">
                {movieGenre.length > 0 && <MovieListGenre list={movieGenre}/>}
        </section>
    )
}

export {routeMain};

export default MovieGenrePage;