import React, {useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";

import routeMain from "./routes";

import { loadMovies } from "store/movies/actions";
import { selectList } from "store/movies/selectors";
import { TypedDispatch } from "store";
import MovieList from "components/MovieList";

import './styles.scss';

const MainPage = () => {
    const dispatch = useDispatch<TypedDispatch>();
    const movieList = useSelector(selectList);

    useEffect(() => {
        dispatch(loadMovies());
    }, [dispatch])

    return (
        <section className="mainPage">
            <div className="mainPageTitle">
                <h1 className="mainTitle">MOVIESinfo</h1>
                <h3 className="title">Самый популярный портал о фильмах</h3>
            </div>
                {movieList.length > 0 && <MovieList list={movieList.slice(0,8)}/>}
        </section>
    )
}

export {routeMain};

export default MainPage;