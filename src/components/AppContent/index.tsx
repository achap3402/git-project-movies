import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import Header from "components/Header";
import Footer from "components/Footer";
import MainPage, {routeMain as routeMainPage} from "pages/MainPage";
import MovieGenrePage, {routeMain as routeMovieGenrePage} from "pages/MovieGenrePage";
import AboutUs, {routeMain as routeAboutUs} from "pages/AboutUs";
import Search, {routeMain as routeSearch} from "pages/Search";
import MovieDetailPage, {routeMain as routeMovieDetailPage} from "pages/MovieDetailPage";

import './styles.scss';

const AppContent = () => {
    return (
        <div className="appContentWrapper">
            <div className="appContent">
                <Header/>
                <main className="mainContent">
                    <Switch>
                        <Route exact path={routeMainPage()} component={MainPage}/>
                        <Route exact path={routeMovieGenrePage()} component={MovieGenrePage}/>
                        <Route exact path={routeAboutUs()} component={AboutUs}/>
                        <Route exact path={routeSearch()} component={Search}/>
                        <Route exact path={routeMovieDetailPage()} component={MovieDetailPage}/>
                        <Redirect
                            to={{
                                pathname: routeMainPage()
                            }}
                        />
                    </Switch>
                </main>
                <Footer/>
            </div>
        </div>
    )
}

export default AppContent;