import React from "react";
import {NavLink} from "react-router-dom";

import {routeMain as routeMovieDetail} from "pages/MovieDetailPage";
import { IMovieGenre } from "types/IMovieGenre";

import Image from "assets/img/star.png";

import './styles.scss';

interface IMovieItemGenreParams {
    item: IMovieGenre;
}

const MovieGenreItem: React.FC<IMovieItemGenreParams> = ({item}) => {
    console.log(item);
    return (
        <div className="movieItemGenreWrapper">
            <NavLink className="movieItemGenre" to={routeMovieDetail(item.show.id)}>
                <div className="image">
                    <img src={item.show.image ? item.show.image.medium : Image} alt={""}/>
                </div>
                <div className="description">
                    <div className="name">
                        <p>{item.show.name}</p>
                    </div>
                    <div className="genres">
                        <p>{item.show.genres.join(", ")}</p>
                    </div>
                </div>
            </NavLink>
        </div>
    )
}

export default MovieGenreItem;