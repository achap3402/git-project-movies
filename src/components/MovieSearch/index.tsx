import React from 'react';

import { IMovieGenre } from 'types/IMovieGenre';
import MovieGenreItem from '../MovieGenreItem';

import './styles.scss';

interface IMovieSearchParams {
    list: IMovieGenre[];
}

const MovieSearch: React.FC<IMovieSearchParams> = ({list}) => {
    return (
    <div className="movieSearchWrapper">
        <div className='movieSearch'>
            {list.map((movie: IMovieGenre) => (
                <MovieGenreItem key={movie.show.id} item={movie}/>
            ))}
        </div>
    </div>
    )
}

export default MovieSearch;