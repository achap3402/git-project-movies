import React from 'react';

import MovieItem from './components/MovieItem';
import { IMovieDetail } from 'types/IMovieDetail';

import './styles.scss';

interface IMovieListParams {
    list: IMovieDetail[];
}

const MovieList: React.FC<IMovieListParams> = ({list}) => {
    return (
    <div className="movieList">
        {list.map((movie: IMovieDetail) => (
            <MovieItem key={movie.id} item={movie}/>
        ))}
    </div>

    )

}


export default MovieList;