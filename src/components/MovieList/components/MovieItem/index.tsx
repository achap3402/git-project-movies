import React from "react";
import {NavLink} from "react-router-dom";

import {routeMain as routeMovieDetailPage} from "pages/MovieDetailPage";
import { IMovieDetail } from "types/IMovieDetail";

import './styles.scss';

interface IMovieItemParams {
    item: IMovieDetail;
}

const MovieItem: React.FC<IMovieItemParams> = ({item}) => {
    return (
        <NavLink className="movieItem" to={routeMovieDetailPage(item.id)}>
            <div className="image">
            <img src={item.image.medium} alt={item.image.medium}/>
            </div>
            <div className="bottomWrapper">
                <div className="name">
                <p>{item.name}</p>
                </div>
                <div className="country">
                {item.network === null ? (
                    <p></p>
                    ) : <p>{item.network.country.name}</p>}
                </div>
                <div className="genres">
                <p>{item.genres.join(", ")}</p>
                </div>
            </div>
        </NavLink>
    )
}

export default MovieItem;