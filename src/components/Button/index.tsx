import React from 'react';

import Vector from 'assets/img/search.png';

interface IButtonParams {
    handleClick: () => void;
} 

const Button: React.FC<IButtonParams> = ({handleClick}) => (
    <button onClick={handleClick}><img src={Vector} alt={Vector} /></button>
    
)

export default Button;