import React from "react";

import './styles.scss';

const Footer = () => {
    return (
        <footer className="mainFooterWrapper">
            <div className="mainFooter">
                <div className="logo left">
                    <div className="logoEllipse">
                    </div>
                    <div className="logoTitle">MOVIESinfo</div>
                </div>
                <div className="middle">&copy; 2 0 2 3</div>
                <div className="right">
                    <div className="small">Made by</div>
                    Alexey Chaplygin
                </div>
            </div>
        </footer>
    )
}

export default Footer;