import React from 'react';

import './styles.scss';

interface IInputParams {
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onKeyDown: (event: any) => void;
    placeholder?: string;
} 

const Input: React.FC<IInputParams> = ({onKeyDown, onChange, placeholder}) => (
    <input onKeyDown={onKeyDown} onChange={onChange} placeholder={placeholder}/>
)

export default Input;