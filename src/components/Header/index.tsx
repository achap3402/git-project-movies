import { useState } from "react";
import { NavLink } from "react-router-dom";

import {routeMain as routeMainPage} from "pages/MainPage";
import {routeMain as routeMovieGenre} from "pages/MovieGenrePage";
import {routeMain as routeAboutUs} from "pages/AboutUs";
import {routeMain as routeSearch} from "pages/Search";

import './styles.scss';

const Header = () => {
  const [active, setActive] = useState(false);

  const toggle = () => {
    setActive(!active);
  }
    return (
      <header className="header">
        <div className="mainHeader">
          <div className="logo">
              <div className="logoEllipse">
              </div>
              <div className="logoTitle">
                  MOVIESinfo
              </div>
          </div>
          <div className={!active ? 'header-burger' : 'header-burger header-burger--active'} onClick={() => toggle()}>
            <span></span>
          </div>
          <nav className={!active ? 'header-menu' : 'header-menu header-menu--active'}>
            <NavLink to={routeMainPage()} onClick={() => toggle()} activeClassName={'linkActive'}>
              Главная
            </NavLink>
            <NavLink to={routeMovieGenre()} onClick={() => toggle()} activeClassName={'linkActive'}>
              Фильмы по категории
            </NavLink>
            <NavLink to={routeAboutUs()} onClick={() => toggle()} activeClassName={'linkActive'}>
              О нас 
            </NavLink>
            <NavLink to={routeSearch()} onClick={() => toggle()} activeClassName={'linkActive'}>
              Поиск
            </NavLink>
          </nav>
        </div>
      </header>
    )
}

export default Header;