import React from 'react';

import MovieGenreItem from '../MovieGenreItem';

import { IMovieGenre } from 'types/IMovieGenre';

import './styles.scss';

interface IMovieListParams {
    list: IMovieGenre[];
}

const MovieListGenre: React.FC<IMovieListParams> = ({list}) => {
    return (
    <div className="movieListWrapper">
        <div className="genreTitle">
            <div className='genre'>
                Выбранная категория:
            </div>
            <p className='genreName'>Mans</p>
        </div>
        <div className='movieGenre'>
            {list.map((movie: IMovieGenre) => (
                <MovieGenreItem key={movie.show.id} item={movie}/>
            ))}
        </div>
    </div>
    )
}

export default MovieListGenre;