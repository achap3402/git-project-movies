import React from 'react';

import prepareDate from 'utils/prepareDate';

interface IDateViewParams {
    value: string;
}

const DateView: React.FC<IDateViewParams> = ({value}) => {
    const {year} = prepareDate(value);
    
    return (
        <p className='year'>{year}</p>
    )
}

export default DateView;