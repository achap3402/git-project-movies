import { IStore } from "./types";

export const selectList = (state: {movieReducer: IStore} ): IStore['list'] => state.movieReducer.list;