import {Dispatch} from 'redux';
import getMovies from "services/getMovie";

import { IStore } from './types';

export const setMovieAction = (list: IStore['list']) => {
    return {
        type: 'movie/setMovie',
        payload: list,
    }
}

export const loadMovies = () => async (dispatch: Dispatch) => {
    try {
        const response = await getMovies();
        dispatch(setMovieAction(response.data))
    } catch(e) {
        console.log(e,'произошла ошибка');
    }
}