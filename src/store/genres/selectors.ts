import { IStore } from "./types";

export const selectGenre = (state: {genreReducer: IStore} ): IStore['list'] => state.genreReducer.list;