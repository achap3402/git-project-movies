import { AnyAction } from "redux";
import { IStore } from "./types";

const initialState = {
    list: [],
}

const genreReducer = (state: IStore = initialState, action: AnyAction) => {
    switch(action.type) {
        case 'movieGenres/setMovieGenres':
            return{...state, list: [...action.payload]}
        default:
            return state;
    }
}

export default genreReducer;