import {Dispatch} from 'redux';
import getGenres from 'services/getGenres';

import { IStore } from './types';

export const setMovieGenresAction = (list: IStore['list']) => {
    return {
        type: 'movieGenres/setMovieGenres',
        payload: list,
    }
}

export const loadMovieGenres = () => async (dispatch: Dispatch) => {
    try {
        const response = await getGenres();
        dispatch(setMovieGenresAction(response.data))
    } catch(e) {
        console.log(e,'произошла ошибка');
    }
}