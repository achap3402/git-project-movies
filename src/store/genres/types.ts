import { IMovieGenre } from "types/IMovieGenre";

export interface IStore {
    list: IMovieGenre[];
}