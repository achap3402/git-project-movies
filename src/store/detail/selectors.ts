import { IStore } from "./types";

export const selectDetail = (state: {detailReducer: IStore}): IStore['movie'] => state.detailReducer.movie;