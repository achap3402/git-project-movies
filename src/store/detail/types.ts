import { IMovieDetail } from "types/IMovieDetail";

export interface IStore {
    movie: IMovieDetail | null;
}