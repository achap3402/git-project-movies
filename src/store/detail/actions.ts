import { Dispatch } from "redux";
import getDetail from "services/getDetail"
import { IStore } from "./types";

export const setDetailAction = (movie: IStore['movie']) => {
    return {
        type: 'detail/setDetail',
        payload: movie,
    }
}

export const loadDetail = (id: string) => async (dispatch: Dispatch) => {
    try {
        const response = await getDetail(id);
        dispatch(setDetailAction(response.data))
    } catch(e) {
        console.log(e, 'произошла ошибка!');
    }
}