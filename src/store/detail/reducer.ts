import { AnyAction } from "redux";
import { IStore } from "./types";

const initialState = {
    movie: null,
}

const detailReducer = (state: IStore = initialState, action: AnyAction) => {
    switch(action.type) {
        case 'detail/setDetail':
            return{...state, movie: action.payload}
        default:
            return state;
    }
}

export default detailReducer;