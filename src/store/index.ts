import {legacy_createStore as createStore, combineReducers, applyMiddleware, AnyAction} from 'redux';
import { composeWithDevTools } from "redux-devtools-extension";
import thunk, { ThunkDispatch } from "redux-thunk";

import movieReducer from './movies/reducer';
import genreReducer from './genres/reducer';
import detailReducer from './detail/reducer';
import categoryReducer from './category/reducer';

const rootReducer = combineReducers({
    movieReducer, genreReducer, detailReducer, categoryReducer
})

const store = createStore(
    rootReducer, composeWithDevTools(applyMiddleware(thunk))
)

export type ReduxState = ReturnType<typeof rootReducer>;
export type TypedDispatch = ThunkDispatch<ReduxState, any, AnyAction>;

export default store;