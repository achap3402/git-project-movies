import { IStore } from "./types";

export const selectCategory = (state: {categoryReducer: IStore} ): IStore['list'] => state.categoryReducer.list;