import {Dispatch} from 'redux';
import getCategory from 'services/getCategory';

import { IStore } from './types';

export const setCategoryAction = (list: IStore['list']) => {
    return {
        type: 'category/setCategory',
        payload: list,
    }
}

export const loadCategory = (genre: string) => async (dispatch: Dispatch) => {
    try {
        const response = await getCategory(genre);
        dispatch(setCategoryAction(response.data))
    } catch(e) {
        console.log(e,'произошла ошибка');
    }
}