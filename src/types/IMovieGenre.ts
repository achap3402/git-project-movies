export interface IMovieGenre {
    show: {
        id: string,
        image: null | {
            medium: string,
            original: string,
        }
        name: string,
        genres: string[],
        network: null | {
            country: {
                name: string,
            }
        }
        summary: string,
    }
}