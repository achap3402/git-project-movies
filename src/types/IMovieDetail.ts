export interface IMovieDetail {
    id: string,
    image: {
        medium: string,
        original: string,
    }
    name: string,
    genres: string[],
    network: null | {
        country: {
            name: string,
        }
    }
    premiered: string,
    rating: {
        average: null | number,
    }
    summary: string,
}