import axios, {AxiosResponse} from "axios";

const getCategory = (genre: string): Promise<AxiosResponse> => axios.get(`https://api.tvmaze.com/search/shows?q=${genre}`);

export default getCategory;