import axios, {AxiosResponse} from "axios";

const getMovieGenres = (): Promise<AxiosResponse> => axios.get('https://api.tvmaze.com/search/shows?q=mans');

export default getMovieGenres;