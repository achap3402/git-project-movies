import axios, {AxiosResponse} from "axios";

const getMovies = (): Promise<AxiosResponse> => axios.get('https://api.tvmaze.com/shows?q=girls');

export default getMovies;